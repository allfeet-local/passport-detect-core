import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.im4java.core.IM4JavaException;

import java.io.File;
import java.io.IOException;

public class Application {

    static {
       // System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) throws TesseractException, InterruptedException, IOException, IM4JavaException {

        ImageMagicUtils.resizeImage("me_ps.png","ps-analyze.png",1024,768);
        ImageMagicUtils.grayScaleImage("ps-analyze.png","ps-analyze-gray.png");
        ImageMagicUtils.cutImage("ps-analyze-gray.png","ps-analyze-gray-1.png",1024,768/2,0,0);
        ImageMagicUtils.cutImage("ps-analyze-gray.png","ps-analyze-gray-2.png",1027,768/2,0,768/2);

        System.out.println("First name is: " + getFirstName("ps-analyze-gray-2.png"));
        System.out.println("Second name is: " + getSecondName("ps-analyze-gray-2.png"));
        System.out.println("Middle name is: " + getMiddleName("ps-analyze-gray-2.png"));
        System.out.println("Sex is: " + getMiddleName("ps-analyze-gray-2.png"));

        System.out.println("Serial number is: " + getSerialAndNumberPassport("ps-analyze-gray-1.png"));

    }

    private static String getSex(String filename) throws InterruptedException, IOException, IM4JavaException, TesseractException {
        ImageMagicUtils.cutImage(filename,"first-name.png",100,20,310,430);
        ImageMagicUtils.colorSpaceImage("first-name.png","first-name.png",5);
        return TesseractUtils.runTesseractWords("first-name.png");
    }

    private static String getSerialAndNumberPassport(String fileName) throws InterruptedException, IOException, IM4JavaException, TesseractException {
        ImageMagicUtils.rotateImage(fileName,270.00);
        ImageMagicUtils.cutImage(fileName,"serial-number.png",200,40,90,30);
        ImageMagicUtils.colorSpaceImage("serial-number.png","serial-number.png",13);
        return TesseractUtils.runTesseractDigits("serial-number.png");
    }

    private static String getFirstName(String filename) throws InterruptedException, IOException, IM4JavaException, TesseractException {
        ImageMagicUtils.cutImage(filename,"first-name.png",100,20,310,430);
        ImageMagicUtils.colorSpaceImage("first-name.png","first-name.png",5);
        return TesseractUtils.runTesseractWords("first-name.png");
    }

    private static String getSecondName(String filename) throws InterruptedException, IOException, IM4JavaException, TesseractException {
        ImageMagicUtils.cutImage(filename,"second-name.png",160,30,290,520);
        ImageMagicUtils.colorSpaceImage("second-name.png","second-name.png",5);
        return TesseractUtils.runTesseractWords("second-name.png");
    }

    private static String getMiddleName(String filename) throws InterruptedException, IOException, IM4JavaException, TesseractException {
        ImageMagicUtils.cutImage(filename,"middle-name.png",120,20,330,490);
        ImageMagicUtils.colorSpaceImage("middle-name.png","middle-name.png",5);
        return TesseractUtils.runTesseractWords("middle-name.png");
    }

}
