import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;

public class TesseractUtils {

    public static String runTesseractWords(String fileName) throws TesseractException {
        File imageFile = new File(fileName);
        ITesseract instance = new Tesseract();
        instance.setLanguage("rus");
        instance.setTessVariable("tessedit_char_whitelist", "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ");
        return instance.doOCR(imageFile);
    }

    public static String runTesseractDigits(String fileName) throws TesseractException {
        File imageFile = new File(fileName);
        ITesseract instance = new Tesseract();
        instance.setLanguage("rus");
        instance.setTessVariable("tessedit_char_whitelist", "1234567890");
        return instance.doOCR(imageFile);
    }
}
