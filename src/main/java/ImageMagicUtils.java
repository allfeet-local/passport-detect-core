import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;

import java.io.IOException;

public class ImageMagicUtils {

    public static void cutImage(String sourceFile, String resultFile, int x1, int y1, int x2, int y2) throws InterruptedException, IOException, IM4JavaException {
        IMOperation op = new IMOperation();
        op.addImage(sourceFile);
        op.crop(x1,y1,x2,y2);
        op.addImage(resultFile);
        performCommand(op);
    }

    public static void resizeImage(String sourceFile, String resultFile, int x, int y) throws InterruptedException, IOException, IM4JavaException {
        IMOperation op = new IMOperation();
        op.addImage(sourceFile);
        op.resize(x,y);
        op.addImage(resultFile);
        performCommand(op);
    }

    public static void grayScaleImage(String sourceFile, String resultFile) throws InterruptedException, IOException, IM4JavaException {
        IMOperation op = new IMOperation();
        op.addImage(sourceFile);
        op.type("Grayscale");
        op.addImage(resultFile);
        performCommand(op);
    }

    public static void colorSpaceImage(String sourceFile, String resultFile, int gr) throws InterruptedException, IOException, IM4JavaException {

        IMOperation op = new IMOperation();
        op.addImage(sourceFile);
        op.colorspace("gray");
        op.negate();
        op.lat(10,10,gr,true);
        op.negate();
        op.addImage(resultFile);
        performCommand(op);
    }

    public static void rotateImage(String sourceFile, Double rotate) throws InterruptedException, IOException, IM4JavaException {

        IMOperation op = new IMOperation();
        op.addImage(sourceFile);
        op.rotate(rotate);
        op.addImage(sourceFile);
        performCommand(op);
    }

    private static void performCommand(IMOperation operation) throws InterruptedException, IOException, IM4JavaException {
        ConvertCmd convertCmd = new ConvertCmd();
       //todo change to logger
       // System.out.print(convertCmd.getCommand()+" ");
       // System.out.println(operation.getCmdArgs());
        convertCmd.run(operation);
    }


}
